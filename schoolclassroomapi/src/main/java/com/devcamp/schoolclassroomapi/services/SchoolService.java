package com.devcamp.schoolclassroomapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.schoolclassroomapi.model.School;

@Service
public class SchoolService {
    @Autowired
    public ClassroomService classroomService = new ClassroomService();

    School school1 = new School(01,"Science and technology school","Le Duan Street", classroomService.getClassroomList1() );
    School school2 = new School(02,"Language University","Ngo Thanh Ton Street", classroomService.getClassroomList2() );
    School school3 = new School(03,"Social Colleague","Quang Trung Street", classroomService.getClassroomList3() );

    //get all schools
    public ArrayList<School> getAllSchools(){
        ArrayList<School> allSchools = new ArrayList<>();
        allSchools.add(school1);
        allSchools.add(school2);
        allSchools.add(school3);
        return allSchools;
    }

    //get school by id
    public School getSchoolById(int id){
        ArrayList<School> allSchools = getAllSchools();
        School findSchool = new School();
        for (School schoolElement : allSchools) {
            if (schoolElement.getId() == id) {
                findSchool = schoolElement;
            }
        }

        return findSchool;
    
    }

    //get School by No Students
    public ArrayList<School> getSchoolsByNoNumber(int noNumber){
        ArrayList<School> schools = new ArrayList<>();
        for (int i=0; i < getAllSchools().size(); i++){
            if (getAllSchools().get(i).getTotalStudent() > noNumber){
                schools.add(getAllSchools().get(i));
            }
        }
        return schools;
    }

}
