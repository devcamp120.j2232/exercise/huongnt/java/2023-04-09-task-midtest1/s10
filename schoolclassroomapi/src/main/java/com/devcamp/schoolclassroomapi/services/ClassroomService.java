package com.devcamp.schoolclassroomapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.schoolclassroomapi.model.Classroom;

@Service


public class ClassroomService {
    Classroom classroom1 = new Classroom(1, "Math", 10);
    Classroom classroom2 = new Classroom(2, "Technology", 20);
    Classroom classroom3 = new Classroom(3, "Science", 12);
    Classroom classroom4 = new Classroom(4, "English", 25);
    Classroom classroom5 = new Classroom(5, "Japanese", 10);
    Classroom classroom6 = new Classroom(6, "Chinese", 15);
    Classroom classroom7 = new Classroom(7, "Philosophy", 15);
    Classroom classroom8 = new Classroom(8, "Culture", 15);
    Classroom classroom9 = new Classroom(9, "Literature", 15);


    public ArrayList<Classroom> getClassroomList1(){
        ArrayList<Classroom> scienceClass = new ArrayList<>();
        scienceClass.add(classroom1);
        scienceClass.add(classroom2);
        scienceClass.add(classroom3);
        return scienceClass;
    }

    public ArrayList<Classroom> getClassroomList2(){
        ArrayList<Classroom> languageClass = new ArrayList<>();
        languageClass.add(classroom4);
        languageClass.add(classroom5);
        languageClass.add(classroom6);
        return languageClass;
    }

    public ArrayList<Classroom> getClassroomList3(){
        ArrayList<Classroom> socialClass = new ArrayList<>();
        socialClass.add(classroom7);
        socialClass.add(classroom8);
        socialClass.add(classroom9);
        return socialClass;
    }
    
    //get all classes
    public ArrayList<Classroom> getAllClasses(){
        ArrayList<Classroom> allClasses = new ArrayList<>();
        allClasses.add(classroom1);
        allClasses.add(classroom2);
        allClasses.add(classroom3);
        allClasses.add(classroom4);
        allClasses.add(classroom5);
        allClasses.add(classroom6);
        allClasses.add(classroom7);
        allClasses.add(classroom8);
        allClasses.add(classroom9);
        return allClasses;

    }

    //get class by noNumber
    public ArrayList<Classroom> getClassByNoNumber( int noStudent) {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        for (int i=0; i< getAllClasses().size(); i++){
            if (getAllClasses().get(i).getNoStudent() >= noStudent){
                classrooms.add(getAllClasses().get(i));
            }
        }
        return classrooms;

    }

}
