package com.devcamp.schoolclassroomapi.model;

import java.util.ArrayList;


public class School extends Classroom{
    private int id;
    private String name;
    private String address;
    private ArrayList<Classroom> classroom;

   public School(){}

    public School(int id, String name, String address, ArrayList<Classroom> classroom) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.classroom = classroom;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public ArrayList<Classroom> getClassrooms() {
        return classroom;
    }
    public void setClassrooms(ArrayList<Classroom> classroom) {
        this.classroom = classroom;
    }
    public int getTotalStudent(){
        int totalStudents = 0;
        for (Classroom classroom : classroom) {
            totalStudents += classroom.getNoStudent();
        }
        return totalStudents;
    }
}
