package com.devcamp.schoolclassroomapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.schoolclassroomapi.model.Classroom;
import com.devcamp.schoolclassroomapi.services.ClassroomService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ClassController {
    @Autowired
    public ClassroomService classroomService;

    //api get all classes
    @GetMapping("/classrooms")
    public ArrayList<Classroom> getAllClassrooms(){
        ArrayList<Classroom> allClassroomsList = classroomService.getAllClasses();
        return allClassroomsList;
    }

    //api get all classes if No Students is bigger than param
    @GetMapping("/class-by-number")
    public ArrayList<Classroom> getNoStudentClass(@RequestParam (name="noNumber", required = true)int noNumber){
        ArrayList<Classroom> ClassroomsList = classroomService.getClassByNoNumber(noNumber);
        return ClassroomsList;

    }
    
}
