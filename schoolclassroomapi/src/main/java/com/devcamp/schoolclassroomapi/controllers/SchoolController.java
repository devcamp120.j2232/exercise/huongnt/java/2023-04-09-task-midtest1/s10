package com.devcamp.schoolclassroomapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.schoolclassroomapi.model.School;
import com.devcamp.schoolclassroomapi.services.SchoolService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class SchoolController {
    @Autowired
    public SchoolService schoolService;

    @GetMapping("/schools")
    public ArrayList<School> getAllSchools() {
        ArrayList<School> allSchoolsList = schoolService.getAllSchools();
        return allSchoolsList;
    }

    // api get school by id
    @GetMapping("/school-info")
    public School getSchoolById(@RequestParam(name = "id", required = true) int id) {
        School findSchool = schoolService.getSchoolById(id);
        return findSchool;
    }

    // api get school by Number Student
    @GetMapping("/school-by-number")
    public ArrayList<School> getSchoolByNumber(@RequestParam(name = "noNumber", required = true) int noStudent) {
        ArrayList<School> SchoolsList = schoolService.getSchoolsByNoNumber(noStudent);
        return SchoolsList;
    }

}
